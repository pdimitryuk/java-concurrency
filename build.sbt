lazy val akkaV = "2.5.21"
lazy val dispatchV = "0.13.4"

val commonSettings = Seq(
  name := "java-concurrency",
  version := "0.0.1",
  scalaVersion := "2.12.8",
  organization  := "dpp.java.concurrency",
  compileOrder := CompileOrder.JavaThenScala,
  fork in run := true,
  scalacOptions ++= Seq(
    "-deprecation", // Emit warning and location for usages of deprecated APIs.
    "-encoding", "utf-8", // Specify character encoding used by source files.
    "-explaintypes", // Explain type errors in more detail.
    "-feature", // Emit warning and location for usages of features that should be imported explicitly.
  )
)


lazy val javaConcurrency = (project in file("."))
  .settings(commonSettings: _*)
  .settings(
    libraryDependencies ++= Seq(
      "com.typesafe.akka" %% "akka-actor" % akkaV,
      "com.typesafe.akka" %% "akka-stream" % akkaV,
      "com.typesafe.akka" %% "akka-testkit" % akkaV % Test
    )
  )

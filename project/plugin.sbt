addSbtPlugin("org.scalariform"  % "sbt-scalariform"      % "1.8.2" )
addSbtPlugin("net.virtual-void" % "sbt-dependency-graph" % "0.9.2")
addSbtPlugin("com.typesafe.sbt" % "sbt-native-packager"  % "1.3.15")
addSbtPlugin("io.get-coursier"  % "sbt-coursier"         % "1.0.3")
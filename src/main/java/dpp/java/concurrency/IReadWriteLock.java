
package dpp.java.concurrency;

import dpp.java.concurrency.utils.ConcurrencyUtils;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class IReadWriteLock extends ConcurrencyUtils {

    public static void main(String[] args) {

        ExecutorService executor = Executors.newFixedThreadPool(2);
        Map<String, String> map = new HashMap<>();
        ReadWriteLock lock = new ReentrantReadWriteLock();

        executor.submit(() -> {
            lock.writeLock().lock();
            try {
                System.out.println("I'm writing...");
                sleep(10);
                map.put("key", "val");
            } finally {
                lock.writeLock().unlock();
            }
        });


        Runnable readTask = () -> {
            lock.readLock().lock();
            try {
                System.out.println("I'm reading...");
                System.out.println(map.get("key"));
                sleep(5);
            } finally {
                lock.readLock().unlock();
            }
        };

        executor.submit(readTask);
        executor.submit(readTask);

        stop(executor);

    }

}


package dpp.java.concurrency;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class DRunnableThreadStarvation {

    public static void main(String[] args) throws InterruptedException {

        ExecutorService executor = Executors.newSingleThreadExecutor();

        executor.submit(() -> {
            System.out.println("First Hello " + Thread.currentThread().getName());

            executor.submit(() -> {
                System.out.println("Second Hello " + Thread.currentThread().getName());

                executor.submit(() -> System.out.println("Third Hello " + Thread.currentThread().getName())); //todo: Is it Warning?

            }); //todo: Is it Warning?

            try {
                TimeUnit.SECONDS.sleep(2);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            System.out.println("Bye " + Thread.currentThread().getName());
        });

        TimeUnit.SECONDS.sleep(5);
        executor.shutdownNow();
    }

}

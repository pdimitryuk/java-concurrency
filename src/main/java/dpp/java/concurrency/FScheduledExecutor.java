
package dpp.java.concurrency;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

public class FScheduledExecutor {

    public static void main(String[] args) throws InterruptedException {

        ScheduledExecutorService executor = Executors.newScheduledThreadPool(1);

        Runnable task = () -> System.out.println("ping: " + System.nanoTime());
        ScheduledFuture<?> future = executor.schedule(task, 10, TimeUnit.SECONDS);

        TimeUnit.MILLISECONDS.sleep(1100);

        long remainingDelay = future.getDelay(TimeUnit.MILLISECONDS);
        System.out.printf("Remaining Delay: %sms\n", remainingDelay);

    }

}

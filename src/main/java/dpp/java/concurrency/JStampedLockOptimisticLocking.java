
package dpp.java.concurrency;

import dpp.java.concurrency.utils.ConcurrencyUtils;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.locks.StampedLock;

public class JStampedLockOptimisticLocking extends ConcurrencyUtils {

    public static void main(String[] args) {

        ExecutorService executor = Executors.newFixedThreadPool(2);
        StampedLock lock = new StampedLock();

        executor.submit(() -> {
            long stamp = lock.tryOptimisticRead();
            try {
                System.out.println("Optimistic Lock Valid: " + lock.validate(stamp));
                sleep(1);
                System.out.println("Optimistic Lock Valid: " + lock.validate(stamp));
                sleep(2);
                System.out.println("Optimistic Lock Valid: " + lock.validate(stamp));
                sleep(2);
            } finally {
                lock.unlock(stamp);
            }
        });

        executor.submit(() -> {
            sleep(2);
            long stamp = lock.writeLock();
            try {
                System.out.println("Write Lock acquired");
            } finally {
                lock.unlock(stamp);
                System.out.println("Write done");
            }
        });

        stop(executor);

    }

}

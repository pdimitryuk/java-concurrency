
package dpp.java.concurrency;

import java.util.concurrent.TimeUnit;

public class AThreadsAndRunnableSleep {

    public static void main(String[] args) {

        Runnable task = () -> {
            try {
                String name = Thread.currentThread().getName();
                System.out.println("A " + name);

                TimeUnit.SECONDS.sleep(10); //Thread.sleep

                System.out.println("B " + name);
            }
            catch (InterruptedException e) { e.printStackTrace(); }
        };

        Thread thread = new Thread(task);
        thread.start();
    }

}

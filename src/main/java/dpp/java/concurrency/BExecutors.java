
package dpp.java.concurrency;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class BExecutors {

    public static void main(String[] args) throws InterruptedException {

        ExecutorService executor = Executors.newSingleThreadExecutor();

        executor.submit(
                () -> System.out.println("Hello " + Thread.currentThread().getName())
        );

        TimeUnit.SECONDS.sleep(1); //Thread.sleep
        System.out.println("I'm still working...");
    }

}


package dpp.java.concurrency;

import java.util.concurrent.*;

public class DCallableThreadStarvation {

    public static void main(String[] args) throws InterruptedException, ExecutionException {

        ExecutorService executor = Executors.newSingleThreadExecutor();

        Callable<Integer> task = () -> {
            try {
                System.out.println("A");
                TimeUnit.SECONDS.sleep(5);

                return executor.submit(() -> {
                    System.out.println("b");
                    return 100;
                }).get(); //todo: Is it Warning?
            }
            catch (InterruptedException e) {
                throw new IllegalStateException("task interrupted", e);
            }
        };

        Future<Integer> future = executor.submit(task);

        System.out.println("result: " + future.get());
    }

}

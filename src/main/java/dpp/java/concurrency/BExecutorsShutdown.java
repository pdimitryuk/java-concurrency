
package dpp.java.concurrency;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class BExecutorsShutdown {

    public static void main(String[] args) {

        ExecutorService executor = Executors.newSingleThreadExecutor();

        executor.submit(() -> {
            String tName = Thread.currentThread().getName();
            System.out.println("Hello " + tName);

            try {
                TimeUnit.SECONDS.sleep(10);
                //TimeUnit.SECONDS.sleep(10); //todo: show this
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            System.out.println("Bye " + tName);
        });

        System.out.println("I'm still working...");

        try {
            System.out.println("attempt to shutdown");
            executor.shutdown();
            executor.awaitTermination(5, TimeUnit.SECONDS);
        }
        catch (InterruptedException e) {
            System.err.println("tasks interrupted");
        }
        finally {
            if (!executor.isTerminated()) {
                System.err.println("cancel non-finished tasks");
            }
            executor.shutdownNow();
            System.out.println("shutdown finished");
        }
    }

}


package dpp.java.concurrency;

import java.util.concurrent.*;

public class CCallableAndFuture {

    public static void main(String[] args) throws ExecutionException, InterruptedException {

        ExecutorService executor = Executors.newFixedThreadPool(1);

        Callable<Integer> task = () -> {
            try {
                TimeUnit.SECONDS.sleep(5);
                return 100;
            }
            catch (InterruptedException e) {
                throw new IllegalStateException("task interrupted", e);
            }
        };

        Future<Integer> future = executor.submit(task);
        System.out.println("future done? " + future.isDone());

//        Integer result = future.get();
//
//        System.out.println("future done? " + future.isDone());
//        System.out.println("result: " + result);

//        executor.shutdownNow();
//        System.out.println("result: " + future.get());

        try {
            Integer result = future.get(3, TimeUnit.SECONDS);
            System.out.println("result: " + result);
        } catch (TimeoutException e) {
            e.printStackTrace();
        }
    }

}


package dpp.java.concurrency;

import dpp.java.concurrency.utils.ConcurrencyUtils;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.IntStream;

public class GSynchronizedProblem extends ConcurrencyUtils {

    private static int count = 0;

    //private static synchronized void increment() {
    private static void increment() {
        synchronized (GSynchronizedProblem.class) {
            count = count + 1;
        }
    }

    public static void main(String[] args) {

        ExecutorService executor = Executors.newFixedThreadPool(10);

        IntStream.range(0, 1000000)
                .forEach(i -> executor.submit(GSynchronizedProblem::increment)); //todo: Race condition

        stop(executor);

        System.out.println("count: " + count);

    }

}

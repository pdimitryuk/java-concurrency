
package dpp.java.concurrency;

import dpp.java.concurrency.utils.ConcurrencyUtils;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.IntStream;

public class LAtomic extends ConcurrencyUtils {

    public static void main(String[] args) {

        AtomicInteger atomicInt = new AtomicInteger(0); //compare-and-swap (CAS)

        ExecutorService executor = Executors.newFixedThreadPool(10);

        IntStream.range(0, 100000)
                .forEach(i -> executor.submit(atomicInt::incrementAndGet));

        stop(executor);

        System.out.println(atomicInt.get());

    }

}

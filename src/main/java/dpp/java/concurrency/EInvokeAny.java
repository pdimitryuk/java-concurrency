
package dpp.java.concurrency;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.*;

public class EInvokeAny {

    static Callable<String> callable(String result, long sleepSeconds) {
        return () -> {
            TimeUnit.SECONDS.sleep(sleepSeconds);
            return result;
        };
    }

    public static void main(String[] args) throws InterruptedException, ExecutionException {

        ExecutorService executor = Executors.newWorkStealingPool();

        List<Callable<String>> callables = Arrays.asList(
                callable("task1", 20),
                callable("task2", 1),
                callable("task3", 30));

        String result = executor.invokeAny(callables);
        System.out.println(result);

    }

}


package dpp.java.concurrency;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class FScheduledExecutorFixedRate {

    public static void main(String[] args) throws InterruptedException {

        ScheduledExecutorService executor = Executors.newScheduledThreadPool(1);

        Runnable task = () -> System.out.println("Ping: " + System.nanoTime());

        executor.scheduleAtFixedRate(task, 1, 2, TimeUnit.SECONDS);

        TimeUnit.MILLISECONDS.sleep(10000);
        executor.shutdownNow();

    }

}

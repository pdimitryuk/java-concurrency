
package dpp.java.concurrency;

public class AThreadsAndRunnable {

    public static void main(String[] args) {

        Runnable task = () -> System.out.println("Hello " + Thread.currentThread().getName());

        new Thread(task)
                .start();

        System.out.println("Done");
    }

}
